<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div id="top-izq">
          <a href="#" target="_self">
          	<img alt="Logo Agencia Tributaria" title="Logo Agencia Tributaria" src="Images/Logo_Agencia.png">
          </a>
      </div>

    </div>
	<div class="nav navbar-nav navbar-right">
        <li><a href="#">Practica 4</a></li>
        <li><a href="#">Grupo 10</a></li>
    </div>
  </div>
</nav>
