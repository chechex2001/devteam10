
<div class="btn-group btn-group-justified" role="group" aria-label="...">
	<div class="btn-group" role="group">
		<?php 
			if($etapa >= 1){ ?>
				<button class="btn btn-primary btn-lg">
		 			Datos
				</button>
		<?php }else{ ?>	
				<button class="btn btn-lg btn-info">
		 			Datos
				</button>
		<?php } ?>		
	</div>
	<div class="btn-group" role="group">
		<?php 
			if($etapa >= 2){ ?>
				<button class="btn btn-primary btn-lg">
		 			Descendientes
				</button>
		<?php }else{ ?>	
				<button class="btn btn-lg btn-info">
		 			Descendientes
				</button>
		<?php } ?>	
	</div>
	<div class="btn-group" role="group">
		<?php 
			if($etapa >= 3){ ?>
				<button class="btn btn-primary btn-lg">
		 			Ascendientes
				</button>
		<?php }else{ ?>	
				<button class="btn btn-lg btn-info">
		 			Ascendientes
				</button>
		<?php } ?>	
	</div>
	<div class="btn-group" role="group">
		<?php 
			if($etapa >= 4){ ?>
				<button class="btn btn-primary btn-lg">
		 			Pensiones
				</button>
		<?php }else{ ?>	
				<button class="btn btn-lg btn-info">
		 			Pensiones
				</button>
		<?php } ?>	
	</div>
	<div class="btn-group" role="group">
		<?php 
			if($etapa >= 5){ ?>
				<button class="btn btn-primary btn-lg">
		 			Pagos
				</button>
		<?php }else{ ?>	
				<button class="btn btn-lg btn-info">
		 			Pagos
				</button>
		<?php } ?>	
	</div>
	<!--<div class="btn-group" role="group">
		<?php 
			if($etapa >= 6){ ?>
				<button class="btn btn-primary btn-lg">
		 			Firma
				</button>
		<?php }else{ ?>	
				<button class="btn btn-lg btn-info">
		 			Firma
				</button>
		<?php } ?>	
	</div>
	<div class="btn-group" role="group">
		<?php 
			if($etapa >= 7){ ?>
				<button class="btn btn-primary btn-lg">
		 			Recibo
				</button>
		<?php }else{ ?>	
				<button class="btn btn-lg btn-info">
		 			Recibo
				</button>
		<?php } ?>	
	</div>-->
</div>