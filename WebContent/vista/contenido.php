<?php 
	if(isset($_GET['etapa']))
		$etapa = $_GET['etapa'];
	else {
		$etapa = 1;
	}
?>
<script>
	var avanzaEtapa = function(aux){
		$('#zona_central').load('vista/contenido.php?etapa='+aux);
		event.preventDefault();
	}
	function completado() {
	    alert("¡Se ha enviado correctamente!");
	}
	function activaCheckbox() {
	    document.getElementById("checkbox_discapacidad").disabled = false;
	}
	function activaNif(aux) {
	    if(aux == 1){
	    	document.getElementById("nifConyuge").disabled = false;
	    }else{
	    	document.getElementById("nifConyuge").disabled = true;
	    }
	}
	
	function aniadeDescendientes() {
	    var table = document.getElementById("tabla");
	    var row = table.insertRow(2);
	    var cell1 = row.insertCell(0);
	    var cell2 = row.insertCell(1);
	   	var cell3 = row.insertCell(2);
	   	var cell4 = row.insertCell(3);
	   	var cell5 = row.insertCell(4);
	   	var cell6 = row.insertCell(5);
	   	
	   	cell3.align = 'center';
	   	cell4.align = 'center';
	   	cell5.align = 'center';
	   	cell6.align = 'center';
	    	    	    	    	    
	    cell1.innerHTML = '<input class="registro" name="situacion" type="number" min="1950" max="2015" value="" placeholder="">';
	    cell2.innerHTML = '<input class="registro" name="situacion" type="number" min="1950" max="2015" value="" placeholder="">';
	    cell3.innerHTML = '<input class="registro" name="situacion" type="radio" value="" placeholder="">';
	    cell4.innerHTML = '<input class="registro" name="situacion" type="radio" value="" placeholder="">';
	    cell5.innerHTML = '<input class="registro" name="situacion" type="checkbox" value="" placeholder="">';
	    cell6.innerHTML = '<input class="registro" name="situacion" type="checkbox" value="" placeholder="">';
	}
	function aniadeAscendiente() {
	    var table = document.getElementById("tabla2");
	    var row = table.insertRow(2);
	    var cell1 = row.insertCell(0);
	    var cell2 = row.insertCell(1);
	   	var cell3 = row.insertCell(2);
	   	var cell4 = row.insertCell(3);
	   	var cell5 = row.insertCell(4);
	   	
	   	cell3.align = 'center';
	   	cell4.align = 'center';
	   	cell5.align = 'center';
	   	cell2.align = 'center';
	    	    	    	    	    
	    cell1.innerHTML = '<input class="registro" name="situacion" type="number" min="1950" max="2015" value="" placeholder="">';
	    cell2.innerHTML = '<input class="registro" name="situacion" type="radio" value="" placeholder="">';
	    cell3.innerHTML = '<input class="registro" name="situacion" type="radio" value="" placeholder="">';
	    cell4.innerHTML = '<input class="registro" name="situacion" type="checkbox" value="" placeholder="">';
	    cell5.innerHTML = '<input class="registro" name="situacion" type="checkbox" value="" placeholder="">';
	}

</script>
<div class="container">

	<div class="jumbotron">
		<?php
			include_once 'titulo.php'; 
			include_once 'etapa.php';
			include_once 'formulario.php'
		?>


	</div>

</div> <!-- /container -->	