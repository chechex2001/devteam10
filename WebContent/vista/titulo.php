<div class="col-sm-12">
  <div class="list-group">
    <a href="#" class="list-group-item active">
      <h4 class="list-group-item-heading">Impuesto sobre la Renta de las Personas Físicas</h4>
      <p class="list-group-item-text">Retenciones sobre rendimientos del trabajo - Comunicación de datos al pagador. <span class="glyphicon glyphicon-info-sign" title="Si prefiere no comunicar a la empresa o entidad pagadora alguno de los datos a que se refiere este modelo, la retención que se le practique podría resultar superior a la procedente. En tal caso, podrá recuperar la diferencia, si procede, al presentar su declaración del IRPF correspondiente al ejercicio de que se trate.
Atención: la inclusión de datos falsos, incompletos o inexactos en esta comunicación, así como la falta de comunicación de variaciones en los mismos que, de haber sido conocidas por el pagador, hubieran determinado una retención superior, constituye infracción tributaria sancionable con multa del 35 al 150 por 100 de las cantidades que se hubieran dejado de retener por esta causa. (Artículo 205 de la Ley 58/2003, de 17 de diciembre, General Tributaria)."></span></p>
    </a>
  </div>
</div> 
