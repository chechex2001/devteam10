

<?php 
	if($etapa == 1){ ?> <!-- Datos -->
		<div class="panel panel-primary">
			<div class="panel-heading">
	  			<h3 class="panel-title">Datos del perceptor que efectúa la comunicación</h3>
			</div>
			<div class="panel-body">
	  			<form name="register" onsubmit="avanzaEtapa(2)" method="POST" accept-charset="utf-8">
					<fieldset id="leyenda">
						<legend>Información Personal </legend>
						<input class="registro" placeholder="NIF" type="text" name="Nif" required pattern="[0-9]{8}[A-Za-z]{1}" title="El formato debe ser 8 números y 1 letra">
						<input class="registro" placeholder="Nombre" type="text" name="nombre" required pattern="[A-Za-z]+" title="El formato debe ser solo letras">
						<input class="registro" placeholder="Apellidos" type="text" name="apellidos" required pattern="[A-Za-z]+" title="El formato debe ser solo letras">
						<input class="registro" placeholder="Año de nacimiento" type="text" name="apellidos" required pattern="[0-9]{4}"  title="El formato debe ser aaaa">
						</br></br>
					</fieldSet>
					<fieldset id="leyenda">
						<legend>Situación familiar </legend>
						<label><input required name="situ" onclick="activaNif(0)" name="situacion" type="radio" value="">Soltero/a, viudo/a, divorciado/a o separado/a</label>  <span class="glyphicon glyphicon-info-sign blue" title="Soltero/a, viudo/a, divorciado/a o separado/a legalmente con hijos solteros menores de 18 años o incapacitados judicialmente y sometidos a patria potestad prorrogada o rehabilitada que conviven exclusivamente con Vd., sin convivir también con el otro progenitor, siempre que proceda consignar al menos un hijo o descendiente en el apartado 2 de este documento"></span></br>
						<label><input required name="situ" onclick="activaNif(1)" name="situacion" type="radio" value="">Casado/a</label>
						<input class="registro" placeholder="NIF cónyuge" id="nifConyuge" disabled type="text" name="Nif" pattern="[0-9]{8}[A-Za-z]{1}" title="El formato debe ser 8 números y 1 letra"> <span class="glyphicon glyphicon-info-sign blue" title="Casado/a y no separado/a legalmente cuyo cónyuge no obtiene rentas superiores a 1.500 euros anuales, excluidas las exentas" ></span></br>
						<label><input required name="situ" onclick="activaNif(0)" name="situacion" type="radio" value="">Otros</label>  <span class="glyphicon glyphicon-info-sign blue" title="Situación familiar distinta de las dos anteriores (solteros sin hijos, casados cuyo cónyuge obtiene rentas superiores a 1.500 euros anuales, ..., etc.) (Marque también esta casilla si no desea manifestar su situación familiar)." ></span></br>
						
						</br>
					</fieldSet>	
					<fieldset id="leyenda">
						<legend>Discapacidad <small>(grado de minusvalía reconocido)</small></legend>
						<label><input name="registro" name="situacion" onclick="activaCheckbox()" type="radio" value="">Igual o superior al 33% e inferior al 65%</label></br>
						<label><input name="registro" name="situacion" onclick="activaCheckbox()" type="radio" value="">Igual o superior al 65%</label></br>
						<label><input name="registro" disabled name="situacion" type="checkbox" value="" id="checkbox_discapacidad">Además, tengo acreditada la necesidad de ayuda de terceras personas o movilidad reducida</label></br>
						
						</br>
					</fieldSet>
					<fieldset id="leyenda">
						<legend>Movilidad geográfica</legend>
						<input class="registro" placeholder="" type="date" name="apellidos">  <span class="glyphicon glyphicon-info-sign blue" title="Si anteriormente estaba Vd. en situación de desempleo e inscrito en la oficina de empleo y la aceptación del puesto de trabajo actual ha exigido
el traslado de su residencia habitual a un nuevo municipio, indique la fecha de dicho traslado"></span>					
						</br>
					</fieldSet>	</br>	
			
					<div>
						<input type="submit" class="btn-xs btn-primary" value="Siguiente">
						<input type="reset" type="button" class="btn-xs btn-primary" value="Borrar">
					</div>
				</form>
			</div>
	  	</div>
<?php }else if($etapa == 2){ ?>	<!-- Descendientes -->
		<div class="panel panel-primary">
			<div class="panel-heading">
	  			<h3 class="panel-title">Hijos y otros descendientes menores de 25 años, o mayores de dicha edad si son discapacitados, que conviven con el perceptor</h3>
  			</div>
		<div class="panel-body">
  			<form action="#" onsubmit="avanzaEtapa(3)" name="register" method="POST" accept-charset="utf-8">
				<fieldset id="leyenda">
					<legend>Descendientes</legend>
					Datos de los hijos o descendientes menores de 25 años (o mayores de dicha edad si son discapacitados) que conviven con Vd. y que no tienen rentas anuales superiores a 8.000 euros.
					</br>
					<div class="table-responsive">
						<table class='table borderless' id="tabla">
							<tr>
								<td>Año de nacimiento</td>
								<td>Año de adopción</td>
								<td>Discapacidad entre 33% y 65%</td>
								<td>Discapacidad superior al 65%</td>
								<td>Ademas, necesita ayuda de terceras personas <span class="glyphicon glyphicon-info-sign blue" title="Si alguno de los hijos o descendientes tiene reconocido un grado de minusvalía igual o superior al 33 por 100, marque con una 'X' la/s casilla/s que corresponda/n a su situación"></span></td>
								<td>Cómputo por entero de hijos o descendientes <span class="glyphicon glyphicon-info-sign blue" title="En caso de hijos que convivan únicamente con Vd., sin convivir también con el otro progenitor (padre o madre), o de nietos que convivan únicamente con Vd., sin convivir también con ningún otro de sus abuelos, indíquelo marcando con una "X" esta casilla."></span></td>
							</tr>
							<tr>
								<td><input class="registro" name="situacion" type="number" value="" min="1950" max="2015" placeholder=""></td>
								<td><input class="registro" name="situacion" type="number" value="" min="1950" max="2015" placeholder=""></td>
								<td align="center"><input class="registro" name="situacion" type="radio" value="" placeholder=""></td>
								<td align="center"><input class="registro" name="situacion" type="radio" value="" placeholder=""></td>
								<td align="center"><input class="registro" name="situacion" type="checkbox" value="" placeholder=""></td>
								<td align="center"><input class="registro" name="situacion" type="checkbox" value="" placeholder=""></td>
							</tr>
						</table>	
					</div>
					
				</fieldSet>
				<input type="button" onclick="aniadeDescendientes()" class="btn-xs btn-primary" value="+" title="Añade un hijo a la lista">
				</br></br>
				<!-- <input type="submit" class="btn-xs btn-primary" value="Anterior"> -->
				<input type="submit" class="btn-xs btn-primary" value="Siguiente">
				<input type="reset" type="button" class="btn-xs btn-primary" value="Borrar">
			</form>		
		</div>
  	</div>
<?php }else if($etapa == 3){ ?>	<!-- Ascendientes-->
		<div class="panel panel-primary">
			<div class="panel-heading">
	  			<h3 class="panel-title">Ascendientes mayores de 65 años, o menores de dicha edad si son discapacitados, que conviven con el perceptor</h3>
			</div>
			<div class="panel-body">
	  			<form  onsubmit="avanzaEtapa(4)">
				  <fieldset id="leyenda">
					<legend>Ascendientes</legend>
					Datos de los hijos o descendientes menores de 25 años (o mayores de dicha edad si son discapacitados) que conviven con Vd. y que no tienen rentas anuales superiores a 8.000 euros.
					</br>
					<div class="table-responsive">
						<table class='table borderless' id="tabla2">
							<tr>
								<td>Año de nacimiento</td>
								<td>Discapacidad entre 33% y 65%</td>
								<td>Discapacidad superior al 65%</td>
								<td>Ademas, necesita ayuda de terceras personas <span class="glyphicon glyphicon-info-sign blue" title="Si alguno de los ascendientes tiene reconocido un grado de minusvalía igual o superior al 33 por 100,
marque con una "X" la/s casilla/s que corresponda/n a su situación."></span></td>
								<td>Convivencia con otros descendientes <span class="glyphicon glyphicon-info-sign blue" title="Si alguno de los ascendientes convive también, al menos durante la mitad del año, con otros descendientes del mismo grado que Vd., indique en esta casilla el número total de descendientes con los que convive, incluido Vd. (Si los ascendientes sólo conviven con Vd., no rellene esta casilla)."></span></td>
							</tr>
							<tr>
								<td><input class="registro" name="situacion" type="number" value="" min="1950" max="2015" placeholder=""></td>
								<td align="center"><input class="registro" name="situacion" type="radio" value="" placeholder=""></td>
								<td align="center"><input class="registro" name="situacion" type="radio" value="" placeholder=""></td>
								<td align="center"><input class="registro" name="situacion" type="checkbox" value="" placeholder=""></td>
								<td align="center"><input class="registro" name="situacion" type="checkbox" value="" placeholder=""></td>
							</tr>
						</table>	
					</div>
					
				</fieldSet>
				<input type="button" onclick="aniadeAscendiente()" class="btn-xs btn-primary" value="+" title="Añade un hijo a la lista"></br></br>
				  <input type="submit" type="button" class="btn-xs btn-primary"  value="Siguiente">
				  <input type="reset" type="button" class="btn-xs btn-primary" value="Borrar">
				</form>
			</div>
	  	</div>
<?php }else if($etapa == 4){ ?>	<!-- Pensiones -->
		<div class="panel panel-primary">
			<div class="panel-heading">
	  			<h3 class="panel-title">Pensiones compensatorias en favor del cónyuge y anualidades por alimentos en favor de los hijos, fijadas ambas por decisión judicial</h3>
			</div>
			<div class="panel-body">
	  			<form  onsubmit="avanzaEtapa(5)">
	  				<fieldset id="leyenda">
						<legend>Pensión compensatoria en favor del cónyuge.</legend>
						<div class="form-group col-md-4">
						    <div class="input-group">
						      <div class="input-group-addon">€</div>
						      <input type="number" class="form-control" id="exampleInputAmount" placeholder="" min="0" max="10000">
						      <div class="input-group-addon">.00</div>
						    </div> 
						</div>  <span class="glyphicon glyphicon-info-sign blue" title="Importe anual que está Vd. obligado a satisfacer por resolución judicial"></span>
					</fieldSet>
					<fieldset id="leyenda">
						<legend>Anualidades por alimentos en favor de los hijos.</legend> 
						<div class="form-group col-md-4">
						    <div class="input-group">
						      <div class="input-group-addon">€</div>
						      <input type="number" class="form-control" id="exampleInputAmount" placeholder="" min="0" max="10000">
						      <div class="input-group-addon">.00</div>
						    </div> 
						</div>  <span class="glyphicon glyphicon-info-sign blue" title="Importe anual que está Vd. obligado a satisfacer por resolución judicial"></span>
						</br></br>
					</fieldSet>
					
				  	<input type="submit" type="button" class="btn-xs btn-primary"  value="Siguiente">
				  	<input type="reset" type="button" class="btn-xs btn-primary" value="Borrar">
				</form>
			</div>
	  	</div>	
<?php }else if($etapa == 5){ ?>	<!-- Recibo -->
		<div class="panel panel-primary">
			<div class="panel-heading">
	  			<h3 class="panel-title">Pagos y Firma</h3>
			</div>
			<div class="panel-body">
	  			<form  onsubmit="completado()">
					<fieldset id="leyenda">
						<legend>Pagos por la adquisición o rehabilitación de la vivienda habitual utilizando financiación ajena</legend>
							Si está Vd. efectuando pagos por préstamos destinados a la adquisición o rehabilitación de su vivienda habitual por los que vaya a tener derecho a deducción por inversión en vivienda habitual en el IRPF
y la cuantía total de sus retribuciones íntegras en concepto de rendimientos del trabajo procedentes de todos sus pagadores es inferior a 33.007,20 euros anuales, marque con una "X" esta casilla
							<input name="registro" name="situacion" type="checkbox" value="" /> <span class="glyphicon glyphicon-info-sign blue" title="Importante: sólo podrán cumplimentar este apartado los contribuyentes que hayan adquirido su vivienda habitual, o hayan satisfecho cantidades por obras de rehabilitación de la misma, antes del 1 de enero de 2013."></span>

						</br></br>
					</fieldSet>
					<fieldset id="leyenda">
						<legend>Fecha y firma de la comunicación</legend>
						Manifiesto ser contribuyente del IRPF y declaro que son ciertos los datos arriba indicados, presentando ante la empresa o entidad pagadora la presente comunicación de mi situación personal y familiar, o de su variación, a los efectos previstos en el artículo 88 del Reglamento del IRPF.
						
						
						
						</br></br>
					</fieldSet>
					<fieldset id="leyenda">
						<legend>Acuse de recibo</legend>
							<input name="registro" name="situacion" type="text" value="" placeholder="Empresa o Entidad" required=""/>
						
						</br></br>
					</fieldSet>

				  <input type="submit" type="button" class="btn-xs btn-primary"  value="Enviar">
				  <input type="reset" type="button" class="btn-xs btn-primary" value="Borrar">
				</form>
			</div>
	  	</div>								
<?php } ?>		
	