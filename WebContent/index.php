<?php session_start(); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="description" content="Agencia Tributaria - Tu dinero es nuestro ahora">
		<link href="css/index.css" rel="stylesheet" type="text/css"  />
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css"  />
		<link href="css/tooltip-viewport.css" rel="stylesheet" type="text/css"  />
		<meta name="author" content="Miguel Rodriguez">
		<!--<script src="js/tooltip-viewport.js" type="text/javascript"></script>-->
		<script src="js/jquery-2.1.0.min.js" type="text/javascript"></script>
    	<title>Agencia Tributaria - Tu dinero es nuestro ahora</title>
    	<link rel="shortcut icon" type="image/x-icon" href="./Images/icono.ico">

	</head>
	<body id="content">
	
		<div class="cabecera">
			<?php include_once 'vista/cabecera.php'; ?>
		</div>		
		<div class="cuerpo contenido">	
			<div id="zona_central" >
				<?php include_once 'vista/contenido.php'; ?>							
			</div>				
		</div>
    

	</body>
</html>
